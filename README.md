Concordia Gaming's Serious SCP-RP Issue Tracker/Feature Request System
======================================================================

Bug Report
----------

Step 1: Sign in to gitlab.com or sign up.

![Image](https://i.imgur.com/aojG1Bo.png "Sign in or sign up")

![Image](https://i.imgur.com/3L6S7gO.png "Choose an option")

Step 2: Click issues

![Image](https://i.imgur.com/JwzFa6W.png "Issues")

Step 3: Click new issue

![Image](https://i.imgur.com/4CSwtVw.png "New Issue")

Step 4: Select BUG under description

![Image](https://i.imgur.com/Yymr8KZ.png "BUG")

Step 5: Fill out the required information and submit the bug report.

![Image](https://i.imgur.com/pPnoN9J.png "Submit")

Step 6: Congratulations you managed to submit a bug report.
